<?php
/**
 * Author: Robert DeVore | @deviorobert
 * URL: html5blank.com | @html5blank
 * Custom functions, support, custom post types and more.
 */

/*------------------------------------*\
    Theme Support
\*------------------------------------*/

if ( function_exists( 'add_theme_support' ) ) {

    // Add Thumbnail Theme Support.
    add_theme_support( 'post-thumbnails' );
    add_image_size( 'large', 1024, '', true ); // Large Thumbnail.
    add_image_size( 'medium_large', 600, '', true );
    add_image_size( 'medium', 400, '', true ); // Medium Thumbnail.
    add_image_size( 'small', 120, '', true ); // Small Thumbnail.
    add_image_size( 'postal_featured', 480, 315, true ); // Custom Thumbnail the_post_thumbnail('postal_featured');
    add_image_size( 'award', 200, 200, true ); // Custom Thumbnail the_post_thumbnail('award');


    // Enables post and comment RSS feed links to head.
    add_theme_support( 'automatic-feed-links' );

    
    // Enable HTML5 support.
    add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );

    
    // Localisation Support.
    load_theme_textdomain( 'html5blank', get_template_directory() . '/languages' );
}


// Remove unwanted images 
// https://wordpress.stackexchange.com/questions/354378/wordpress-adding-scaled-images-that-dont-exist-1536x1536-and-2048x2048
function remove_default_image_sizes( $sizes) {
    //unset( $sizes['large']);
    //unset( $sizes['thumbnail']);
    //unset( $sizes['medium']);
    //unset( $sizes['medium_large']);
    unset( $sizes['1536x1536']);
    unset( $sizes['2048x2048']);
    return $sizes;
}
add_filter('intermediate_image_sizes_advanced', 'remove_default_image_sizes');

/*------------------------------------*\
    Functions
\*------------------------------------*/

// HTML5 Blank navigation
function html5blank_nav() {
    wp_nav_menu(
    array(
        'theme_location'  => 'header-menu',
        'menu'            => '',
        'container'       => 'div',
        'container_class' => 'menu-{menu slug}-container',
        'container_id'    => '',
        'menu_class'      => 'menu',
        'menu_id'         => '',
        'echo'            => true,
        'fallback_cb'     => 'wp_page_menu',
        'before'          => '',
        'after'           => '',
        'link_before'     => '',
        'link_after'      => '',
        'items_wrap'      => '<ul class="cd-secondary-nav">%3$s</ul>',
        'depth'           => 0,
        'walker'          => '',
        )
    );
}


// HTML5 footer nav
function footer_nav() {
	wp_nav_menu(
	array(
		'theme_location'  => 'footer-menu',
		'menu'            => '',
		'container'       => 'div',
		'container_class' => '',
		'container_id'    => '',
		'menu_class'      => 'menu',
        
		'menu_id'         => '',
		'echo'            => true,
		'fallback_cb'     => 'wp_page_menu',
		'before'          => '',
		'after'           => '',
		'link_before'     => '',
		'link_after'      => '',
		'items_wrap'      => '<ul class="footer-menu">%3$s</ul>',
		'depth'           => 0,
		'walker'          => ''
		)
	);
}


// Register HTML5 Blank Navigation
function register_html5_menu() {
    register_nav_menus( array( // Using array to specify more menus if needed
        'header-menu'  => esc_html( 'Header Menu', 'html5blank' ), // Main Navigation
        'footer-menu' => __('Footer Menu', 'html5blank'), // Footer nav
        'extra-menu'   => esc_html( 'Extra Menu', 'html5blank' ) // Extra Navigation if needed (duplicate as many as you need!)
    ) );
}
add_action( 'init', 'register_html5_menu' ); // Add HTML5 Blank Menu


// Remove the <div> surrounding the dynamic navigation to cleanup markup
function my_wp_nav_menu_args( $args = '' ) {
    $args['container'] = false;
    return $args;
}
add_filter( 'wp_nav_menu_args', 'my_wp_nav_menu_args' );


// Remove Injected classes, ID's and Page ID's from Navigation <li> items
function my_css_attributes_filter( $var ) {
    return is_array( $var ) ? array() : '';
}


// Remove invalid rel attribute values in the categorylist
function remove_category_rel_from_category_list( $thelist ) {
    return str_replace( 'rel="category tag"', 'rel="tag"', $thelist );
}
add_filter( 'the_category', 'remove_category_rel_from_category_list' );


// Add page slug to body class, love this - Credit: Starkers Wordpress Theme
function add_slug_to_body_class( $classes ) {
    global $post;
    if ( is_home() ) {
        $key = array_search( 'blog', $classes, true );
        if ( $key > -1 ) {
            unset( $classes[$key] );
        }
    } elseif ( is_page() ) {
        $classes[] = sanitize_html_class( $post->post_name );
    } elseif ( is_singular() ) {
        $classes[] = sanitize_html_class( $post->post_name );
    }

    return $classes;
}
add_filter( 'body_class', 'add_slug_to_body_class' ); // Add slug to body class (Starkers build)



// If Dynamic Sidebar Exists
if ( function_exists( 'register_sidebar' ) ) {
    // Define Sidebar Widget Area 1
    register_sidebar( array(
        'name'          => esc_html( 'Widget Area 1', 'html5blank' ),
        'description'   => esc_html( 'Description for this widget-area...', 'html5blank' ),
        'id'            => 'widget-area-1',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>',
    ) );

    // Define Sidebar Widget Area 2
    register_sidebar( array(
        'name'          => esc_html( 'Widget Area 2', 'html5blank' ),
        'description'   => esc_html( 'Description for this widget-area...', 'html5blank' ),
        'id'            => 'widget-area-2',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>',
    ) );
}


// Remove wp_head() injected Recent Comment styles
function my_remove_recent_comments_style() {
    global $wp_widget_factory;

    if ( isset( $wp_widget_factory->widgets['WP_Widget_Recent_Comments'] ) ) {
        remove_action( 'wp_head', array(
            $wp_widget_factory->widgets['WP_Widget_Recent_Comments'],
            'recent_comments_style'
        ) );
    }
}
add_action( 'widgets_init', 'my_remove_recent_comments_style' ); // Remove inline Recent Comment Styles from wp_head()


// Create 20 Word Callback for Index page Excerpts, call using html5wp_excerpt('html5wp_index');
function html5wp_index( $length ) {
    return 20;
}


// Create 40 Word Callback for Custom Post Excerpts, call using html5wp_excerpt('html5wp_custom_post');
function html5wp_custom_post( $length ) {
    return 40;
}


// Create the Custom Excerpts callback
function html5wp_excerpt( $length_callback = '', $more_callback = '' ) {
    global $post;
    if ( function_exists( $length_callback ) ) {
        add_filter( 'excerpt_length', $length_callback );
    }
    if ( function_exists( $more_callback ) ) {
        add_filter( 'excerpt_more', $more_callback );
    }
    $output = get_the_excerpt();
    $output = apply_filters( 'wptexturize', $output );
    $output = apply_filters( 'convert_chars', $output );
    $output = '<p>' . $output . '</p>';
    echo esc_html( $output );
}


// Custom View Article link to Post
function html5_blank_view_article( $more ) {
    global $post;
    return '... <a class="view-article" href="' . get_permalink( $post->ID ) . '">' . esc_html_e( 'View Article', 'html5blank' ) . '</a>';
}
add_filter( 'excerpt_more', 'html5_blank_view_article' ); // Add 'View Article' button instead of [...] for Excerpts


// Remove Admin bar
function remove_admin_bar() {
    return false;
}
add_filter( 'show_admin_bar', 'remove_admin_bar' );



// Enable Threaded Comments
function enable_threaded_comments() {
    if ( ! is_admin() ) {
        if ( is_singular() AND comments_open() AND ( get_option( 'thread_comments' ) == 1 ) ) {
            wp_enqueue_script( 'comment-reply' );
        }
    }
}
add_action( 'get_header', 'enable_threaded_comments' );


// Blog Pagination
// http://www.wpbeginner.com/wp-themes/how-to-add-numeric-pagination-in-your-wordpress-theme/
function wp_numeric_posts_nav() {

	if( is_singular() )
		return;

	global $wp_query;

	/** Stop execution if there's only 1 page */
	if( $wp_query->max_num_pages <= 1 )
		return;

	$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
	$max   = intval( $wp_query->max_num_pages );

	/**	Add current page to the array */
	if ( $paged >= 1 )
		$links[] = $paged;

	/**	Add the pages around the current page to the array */
	if ( $paged >= 3 ) {
		$links[] = $paged - 1;
		$links[] = $paged - 2;
	}

	if ( ( $paged + 2 ) <= $max ) {
		$links[] = $paged + 2;
		$links[] = $paged + 1;
	}

	echo '<ul class="pagination">' . "\n";

	/**	Previous Post Link */
	if ( get_previous_posts_link() )
		printf( '<li>%s</li>' . "\n", get_previous_posts_link('&laquo;') );

	/**	Link to first page, plus ellipses if necessary */
	if ( ! in_array( 1, $links ) ) {
		$class = 1 == $paged ? ' class="active"' : '';

		printf( '<li%s><a href="%s" class="page">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

		if ( ! in_array( 2, $links ) )
			echo '<li>…</li>';
	}

	/**	Link to current page, plus 2 pages in either direction if necessary */
	sort( $links );
	foreach ( (array) $links as $link ) {
		$class = $paged == $link ? ' class="active"' : '';
		printf( '<li%s><a href="%s" class="page">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
	}

	/**	Link to last page, plus ellipses if necessary */
	if ( ! in_array( $max, $links ) ) {
		if ( ! in_array( $max - 1, $links ) )
			echo '<li>…</li>' . "\n";

		$class = $paged == $max ? ' class="active"' : '';
		printf( '<li%s><a href="%s" class="page">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
	}

	/**	Next Post Link */
	if ( get_next_posts_link() )
		printf( '<li>%s</li>' . "\n", get_next_posts_link('&raquo;') );

	echo '</ul>' . "\n";

}


// Custom Comments Callback
function html5blankcomments( $comment, $args, $depth ) {
    $GLOBALS['comment'] = $comment;
    extract( $args, EXTR_SKIP );

    if ( 'div' == $args['style'] ) {
        $tag       = 'div';
        $add_below = 'comment';
    } else {
        $tag       = 'li';
        $add_below = 'div-comment';
    }
?>
    <!-- heads up: starting < for the html tag (li or div) in the next line: -->
    <<?php echo esc_html( $tag ) ?> <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ) ?> id="comment-<?php comment_ID(); ?>">
    <?php if ( 'div' != $args['style'] ) : ?>
    <div id="div-comment-<?php comment_ID(); ?>" class="comment-body">
    <?php endif; ?>
    <div class="comment-author vcard">
    <?php if ( $args['avatar_size'] != 0 ) echo get_avatar( $comment, $args['avatar_size'] ); ?>
    <?php printf( esc_html( '<cite class="fn">%s</cite> <span class="says">says:</span>' ), get_comment_author_link() ) ?>
    </div>
<?php if ( $comment->comment_approved == '0' ) : ?>
    <em class="comment-awaiting-moderation"><?php esc_html_e( 'Your comment is awaiting moderation.' ) ?></em>
    <br />
<?php endif; ?>

    <div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>">
        <?php
            printf( esc_html( '%1$s at %2$s' ), get_comment_date(), get_comment_time() ) ?></a><?php edit_comment_link( esc_html_e( '(Edit)' ), '  ', '' );
        ?>
    </div>

    <?php comment_text() ?>

    <div class="reply">
    <?php comment_reply_link( array_merge( $args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ) ?>
    </div>
    <?php if ( 'div' != $args['style'] ) : ?>
    </div>
    <?php endif; ?>
<?php }


/*------------------------------------*\
    Actions + Filters + ShortCodes
\*------------------------------------*/

// Remove Actions
remove_action( 'wp_head', 'feed_links_extra', 3 ); // Display the links to the extra feeds such as category feeds
remove_action( 'wp_head', 'feed_links', 2 ); // Display the links to the general feeds: Post and Comment Feed
remove_action( 'wp_head', 'rsd_link' ); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action( 'wp_head', 'wlwmanifest_link' ); // Display the link to the Windows Live Writer manifest file.
remove_action( 'wp_head', 'wp_generator' ); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action( 'wp_head', 'rel_canonical' );
remove_action( 'wp_head', 'wp_shortlink_wp_head', 10, 0 );

// Add Filters
add_filter( 'widget_text', 'do_shortcode' ); // Allow shortcodes in Dynamic Sidebar
add_filter( 'widget_text', 'shortcode_unautop' ); // Remove <p> tags in Dynamic Sidebars (better!)
add_filter( 'the_excerpt', 'shortcode_unautop' ); // Remove auto <p> tags in Excerpt (Manual Excerpts only)
add_filter( 'the_excerpt', 'do_shortcode' ); // Allows Shortcodes to be executed in Excerpt (Manual Excerpts only)

// Remove Filters
remove_filter( 'the_excerpt', 'wpautop' ); // Remove <p> tags from Excerpt altogether

// Shortcodes
add_shortcode( 'html5_shortcode_demo', 'html5_shortcode_demo' ); // You can place [html5_shortcode_demo] in Pages, Posts now.
add_shortcode( 'html5_shortcode_demo_2', 'html5_shortcode_demo_2' ); // Place [html5_shortcode_demo_2] in Pages, Posts now.

// Shortcodes above would be nested like this -
// [html5_shortcode_demo] [html5_shortcode_demo_2] Here's the page title! [/html5_shortcode_demo_2] [/html5_shortcode_demo]


/*------------------------------------*\
    Custom Post Types
\*------------------------------------*/
// Create 1 Custom Post type for a Demo, called postal
function create_post_type_html5() {
    
    register_taxonomy_for_object_type( 'category', 'postals' ); // Register Taxonomies for Category
    register_taxonomy_for_object_type( 'post_tag', 'postals' );
    register_post_type( 'postals', // Register Custom Post Type
        array(
        'labels'       => array(
            'name'               => esc_html( 'Postals', 'html5blank' ), // Rename these to suit
            'singular_name'      => esc_html( 'Postal', 'html5blank' ),
            'add_new'            => esc_html( 'Add New', 'html5blank' ),
            'add_new_item'       => esc_html( 'Add New Postal', 'html5blank' ),
            'edit'               => esc_html( 'Edit', 'html5blank' ),
            'edit_item'          => esc_html( 'Edit Postal', 'html5blank' ),
            'new_item'           => esc_html( 'New Postal', 'html5blank' ),
            'view'               => esc_html( 'View Postal', 'html5blank' ),
            'view_item'          => esc_html( 'View Postal', 'html5blank' ),
            'search_items'       => esc_html( 'Search Postal', 'html5blank' ),
            'not_found'          => esc_html( 'No Postal found', 'html5blank' ),
            'not_found_in_trash' => esc_html( 'No Postal found in Trash', 'html5blank' ),
            // Overrides the “Featured Image” label
            'featured_image'     => esc_html( 'Imatge destacada de la postal', 'html5blank' ),
            // Overrides the “Set featured image” label
            'set_featured_image' => esc_html( 'Postal imatge destacada', 'html5blank' ),
            // Overrides the “Remove featured image” label
            'remove_featured_image' => esc_html( 'Postal suprimeix imatge destacada', 'html5blank' ),
        ),
        // Custom icon https://developer.wordpress.org/resource/dashicons/
        'menu_icon'    => 'dashicons-cover-image',
        'public'       => true,
        'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
        'has_archive'  => true,
        'supports'     => array(
            'title',
            'editor',
            'excerpt',
            'thumbnail'
        ), // Go to Dashboard Custom HTML5 Blank post for supports
        'can_export'   => true, // Allows export in Tools > Export
        'menu_position'       => 20, // Position on Admin menu
        'taxonomies'   => array(
            'post_tag'//,
            //'category'
        ) // Add Category and Post Tags support
    ) );
    
    
    register_taxonomy_for_object_type( 'category', 'reconeixements' ); // Register Taxonomies for Category
    register_taxonomy_for_object_type( 'post_tag', 'reconeixements' );
    register_post_type( 'reconeixements', // Register Custom Post Type
        array(
        'labels'       => array(
            'name'               => esc_html( 'Reconeixements', 'html5blank' ), // Rename these to suit
            'singular_name'      => esc_html( 'Reconeixement', 'html5blank' ),
            'add_new'            => esc_html( 'Add New', 'html5blank' ),
            'add_new_item'       => esc_html( 'Add New Reconeixement', 'html5blank' ),
            'edit'               => esc_html( 'Edit', 'html5blank' ),
            'edit_item'          => esc_html( 'Edit Reconeixement', 'html5blank' ),
            'new_item'           => esc_html( 'New Reconeixement', 'html5blank' ),
            'view'               => esc_html( 'View Reconeixement', 'html5blank' ),
            'view_item'          => esc_html( 'View Reconeixement', 'html5blank' ),
            'search_items'       => esc_html( 'Search Reconeixement', 'html5blank' ),
            'not_found'          => esc_html( 'No Reconeixement found', 'html5blank' ),
            'not_found_in_trash' => esc_html( 'No Reconeixement found in Trash', 'html5blank' ),
            // Overrides the “Featured Image” label
            'featured_image'     => esc_html( 'Imatge destacada de la reconeixement', 'html5blank' ),
            // Overrides the “Set featured image” label
            'set_featured_image' => esc_html( 'Reconeixement imatge destacada', 'html5blank' ),
            // Overrides the “Remove featured image” label
            'remove_featured_image' => esc_html( 'Reconeixement suprimeix imatge destacada', 'html5blank' ),
        ),
        // Custom icon https://developer.wordpress.org/resource/dashicons/
        'menu_icon'    => 'dashicons-awards',
        'public'       => true,
        'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
        'has_archive'  => true,
        'supports'     => array(
            'title',
            'editor',
            'excerpt',
            'thumbnail'
        ), // Go to Dashboard Custom HTML5 Blank post for supports
        'can_export'   => true, // Allows export in Tools > Export
        'menu_position'       => 21//, // Position on Admin menu
        //'taxonomies'   => array(
        //    'post_tag'//,
            //'category'
        //) // Add Category and Post Tags support
    ) );
}
add_action( 'init', 'create_post_type_html5' ); // Add our HTML5 Blank Custom Post Type


// Add categories to custom post Type
// https://stackoverflow.com/questions/33214580/restrict-category-for-custom-post-type-in-wordpress
function html5_blank_cat() {
    
    register_taxonomy(
        'postals_cats',
        'postals',
        array(
            'label' => __( 'Categories Postals' ),
            //'rewrite' => array( 'slug' => 'sol' ),
            'hierarchical' => true,
            'show_admin_column' => true//,
            //'meta_box_cb'       => false
        )
    );
    
    /*register_taxonomy(
        'member_type',
        'postals',
        array(
            'label' => __( 'Anyada' ),
            'rewrite' => array( 'slug' => 'anyada' ),
            'hierarchical' => true,
            'show_admin_column' => true,
            'meta_box_cb'       => false
        )
    );
    
    register_taxonomy(
        'member_company',
        'postals',
        array(
            'label' => __( 'Tipus de sòl' ),
            'rewrite' => array( 'slug' => 'sol' ),
            'hierarchical' => true,
            'show_admin_column' => true,
            'meta_box_cb'       => false
        )
    );
    
    register_taxonomy(
        'member_keyword',
        'postals',
        array(
            'label' => __( 'Varietat' ),
            'rewrite' => array( 'slug' => 'varietat' ),
            'hierarchical' => true,
            'show_admin_column' => true,
            'meta_box_cb'       => false
        )
    );*/

}
add_action( 'init', 'html5_blank_cat' );


/*------------------------------------*\
    MSC- Custom Functions
\*------------------------------------*/
// Don't load Gutenberg-related stylesheets.
// https://wordpress.org/support/topic/how-would-one-completely-disable-gutenberg-css-in-storefront/
function remove_block_css() {
    wp_dequeue_style( 'wp-block-library' ); // WordPress core
    wp_dequeue_style( 'wp-block-library-theme' ); // WordPress core
    wp_dequeue_style( 'wc-block-style' ); // WooCommerce
    wp_dequeue_style( 'storefront-gutenberg-blocks' ); // Storefront theme
}
add_action( 'wp_enqueue_scripts', 'remove_block_css', 100 );


// remove emoji icons (WP 4.2)
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );


// remove global inline styles
// https://webgaku.net/wordpress/global-styles-inline-css/ & https://perfmatters.io/docs/remove-global-inline-styles-wordpress/
function remove_global_styles(){
    wp_dequeue_style( 'global-styles' );
}
add_action( 'wp_enqueue_scripts', 'remove_global_styles' );


// Dequeue classic-themes.min.css
// https://wordpress.stackexchange.com/questions/410983/dequeue-classic-themes-min-css
function mywptheme_child_deregister_styles() {
    wp_dequeue_style( 'classic-theme-styles' );
}
add_action( 'wp_enqueue_scripts', 'mywptheme_child_deregister_styles', 20 );


// Custom login page
// https://codex.wordpress.org/Customizing_the_Login_Form
function my_login_logo() { ?>
    <style type="text/css">
        body.login {
            background: #ededed;
        }
        .login h1 a {
            background-image: url(<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/vins-de-postal.svg) !important;
            background-position: center center !important;
            background-size: cover !important;
            width: 320px !important;
            height: 54px !important;
            margin: 0 auto 15px !important;
            padding-bottom: 7px !important;
            border: none;
        }
        .login form {
            margin-top: 0 !important;
            border: 2px solid #111111!important;
            background-color: #ffffff !important;
        }
        .login form .input,
        .login form input[type=checkbox],
        .login input[type=text] {
            border: 1px solid #111111 !important;
            border-radius: 0 !important;
        }
        body.login div#login form#loginform p.submit input#wp-submit {
            color: #ffffff !important;
            text-transform: uppercase;
            font-weight: bold;
            background: #111111;
            border-color: #111111;
            border-radius: 0;
            text-shadow: none;
            box-shadow: none;
        }
        body.login div#login form#loginform p.submit input#wp-submit:hover,
        body.login div#login form#loginform p.submit input#wp-submit:focus {
            background: #111111;
            border-color: #111111;
        }
        body.login div#login p#nav a,
        body.login div#login p#backtoblog a,
        body.login .privacy-policy-link {
            color: #111111;
            text-decoration: underline;
        }
        body.login div#login p#nav a:hover,
        body.login div#login p#backtoblog a:hover,
        body.login .privacy-policy-link:hover {
            text-decoration: none;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );

function my_login_logo_url() {
    return home_url();
}
add_filter( 'login_headerurl', 'my_login_logo_url' );

function my_login_logo_url_title() {
    return 'Title | Tagline';
}
add_filter( 'login_headertitle', 'my_login_logo_url_title' );


// Remove language dropdown on login page
add_filter( 'login_display_language_dropdown', '__return_false' );


// Add excerpt to page
add_action( 'init', 'my_add_excerpts_to_pages' );
function my_add_excerpts_to_pages() {
     add_post_type_support( 'page', 'excerpt' );
}


// Advanced Custom Fields options page
// https://www.advancedcustomfields.com/resources/options-page/
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	
}


// Change sub menu class
// https://www.codinghook.com/how-to-change-wordpress-sub-menu-class-with-custom-class/
function replace_submenu_class($menu) {  
    $menu = preg_replace('/ class="sub-menu"/','/ class="yourclassname otherclass" /',$menu);  
    return $menu;  
}  
add_filter('wp_nav_menu','replace_submenu_class');


// Add search form to menu
// https://wordpress.stackexchange.com/questions/205657/how-do-i-add-a-search-box-to-the-nav-menu
// https://www.isitwp.com/add-search-form-to-specific-wp-nav-menu/
add_filter( 'wp_nav_menu_items','add_search_box', 10, 2 );
function add_search_box( $items, $args ) {
    $items .= '<li>' . get_search_form( false ) . '</li>';
    return $items;
}


/**
 * Descriptions on Header Menu
 * @author Bill Erickson
 * @link http://www.billerickson.net/code/add-description-to-wordpress-menu-items/
 * 
 * @param string $item_output, HTML outputp for the menu item
 * @param object $item, menu item object
 * @param int $depth, depth in menu structure
 * @param object $args, arguments passed to wp_nav_menu()
 * @return string $item_output
 */
function be_header_menu_desc( $item_output, $item, $depth, $args ) {
	
	if( 'header-menu' == $args->theme_location && ! $depth && $item->description )
		$item_output = str_replace( '</a>', '<span class="description">' . $item->description . '</span></a>', $item_output );
		
	return $item_output;
}
add_filter( 'walker_nav_menu_start_el', 'be_header_menu_desc', 10, 4 );