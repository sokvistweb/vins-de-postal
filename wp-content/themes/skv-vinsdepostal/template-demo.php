<?php /* Template Name: Demo Page Template */ get_header(); ?>


<main role="main" class="wrapper">
    
    <section class="container">
        
        <h2 class="title"><?php the_title(); ?></h2>
        
        <div class="row">
            <div class="column">
                <?php if ( have_posts()) : while ( have_posts() ) : the_post(); ?>

                <?php the_content(); ?>
                
                <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>  
        
    </section>
    
    
    <section class="container">

        <div class="row">
            <div class="column">
                <!-- Dispaly Custom Post Type (html5-blank in this case) -->
                <?php query_posts('post_type=html5-blank'); while (have_posts ()): the_post(); ?>
                
                <h3><?php the_title() ?></h3>
                
                <?php the_content() ?>
                
                <h4><?php the_excerpt() ?></h4>
                
                <?php endwhile; wp_reset_postdata(); ?>
            </div>
        </div>
        
    </section>
    
</main>


<?php get_sidebar(); ?>

<?php get_footer(); ?>
