    <footer class="sk-footer">
        <div class="cd-container grid-margin-footer">
            <p>CELLER COOPERATIU D'ESPOLLA S.C.C.L.<br> 
            Ctra. de Roses, s/n, 17753 Espolla<br>
            Telèfon: <a href="tel:0034972563178">+34 972 56 31 78</a><br>
            <a href="mailto:vinsdepostal@cellerespolla.com" title="El nostre email">vinsdepostal@cellerespolla.com</a></p>
            <ul class="logos">
                <li><a href="http://www.doemporda.cat/" title="DO Empordà" target="_blank"><span>DO Empordà</span></a></li>
                <li><a href="http://ca.costabrava.org/que-fer/ruta-del-vi" title="Ruta del vi - Costa Brava" target="_blank"><span>Ruta del vi</span></a></li>
                <li><a href="http://www.wineinmoderation.eu/" title="Wine in Moderation" target="_blank"><span>Wine in Moderation</span></a></li>
                <li><a href="https://next-generation-eu.europa.eu/" title="NextGenerationEU" target="_blank"><span>Financiado por la Union Europea</span></a></li>
                <li><a href="https://planderecuperacion.gob.es/" title="PRTR" target="_blank"><span>Plan de Rcuperacion, Transformacion y Resiliencia</span></a></li>
            </ul>
            <ul class="footer-info">
                <li><a href="https://www.cellerespolla.com/avis-legal/" title="Avís Legal" target="_blank">Avís Legal</a></li>
                <li><a href="https://www.cellerespolla.com/politica-de-cookies/" title="Política de cookies" target="_blank">Política de cookies</a></li>
                <li><a href="https://www.cellerespolla.com/politica-de-privacitat/" title="Política de privacitat" target="_blank">Política de privacitat</a></li>
                <li><a href="/declaracio-daccessibilitat/">Declaració d'Accessibilitat</a></li>
                <li>&copy;2015 Celler Espolla</li>
                <li><a href="http://sokvist.com/" title="Sokvist, Web & Seo" target="_blank">Sokvist</a></li>
            </ul>
        </div> <!-- /cd-container -->
    </footer> <!-- /sk-footer -->


<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/js/vendor/jquery-2.1.1.js"></script>
<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/js/js/plugins-min.js"></script>
<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/js/js/main-min.js"></script>

    
</body>
</html>

