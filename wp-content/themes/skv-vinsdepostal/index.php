<?php get_header(); ?>


<main role="main" id="maincontent">
    
    <div class="container" id="cards">
        <h1 class="s-title">Blog page</h1>

        <div class="row">
            <!-- The Loop - Check if there are posts -->
            <?php if (have_posts()): while (have_posts()) : the_post(); ?>
            <div class="column card-column">
                <div class="card">
                    <div class="card-image">
                        <?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
                        <a class="thumbnail" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                        <?php the_post_thumbnail('medium'); ?></a>
                        <?php endif; ?>
                    </div>
                    <div class="card-header">
                        <!-- Display the title -->
                        <div class="card-title"><h2><?php the_title(); ?></h2></div>
                    </div>
                    <!-- Display the excerpt -->
                    <div class="card-body"><?php the_excerpt(); ?></div>
                    <div class="card-footer"><a class="button" href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">Read more</a></div>
                </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
        </div>
        
        <?php wp_numeric_posts_nav(); ?>
    </div>
    
</main>


<?php get_sidebar(); ?>

<?php get_footer(); ?>
