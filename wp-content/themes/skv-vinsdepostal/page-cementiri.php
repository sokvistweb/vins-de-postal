<?php /* Template Name: Page Cementiri */ get_header(); ?>


<main role="main" id="maincontent" class="wrapper">
    
    <section class="container padding-x-short">
        <!-- This is the page title (any page using Default templagte, as this is page.php template) -->
        <h2 class="title text-center"><?php the_title(); ?></h2>
        
        <div class="row">
            <div class="column jenna-sue text-center">
                <!-- The Loop - Check if there are posts -->
                <?php if ( have_posts()) : while ( have_posts() ) : the_post(); ?>
                <!-- Dispaly the content of the current post -->
                <?php the_content(); ?>
                
                <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>
        
        
        <div class="postcards" id="postals">
            
            <div class="search-wrapper">
                <input type="search" class="search search-by-word" placeholder="Cerca per Anyada, Varietat o Tipus de sòl" />
            </div>
            
            <div class="grid list">
                <?php if (have_posts()) : ?>
                <?php query_posts(array(
                    'post_type' => 'postals',
                    'posts_per_page' => -1,
                    'tax_query' => array(
                        array (
                            'taxonomy' => 'postals_cats',
                            'field' => 'slug',
                            'terms' => 'cementiri'
                        )
                    )
                )); ?>
                <?php while (have_posts()) : the_post(); ?>
                <a href="<?php the_permalink(); ?>">
                    <figure class="two-cols">
                    <?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
                        <?php the_post_thumbnail('postal_featured', array( 
                    'loading' => 'eager'
                    )); ?>
                    <?php endif; ?>
                        <div class="img-overlay"><span>+</span></div>
                    </figure>
                    <div class="hidden-searchcontent">
                        <div class="anyada"><?php the_field( 'anyada' ); ?></div>
                        <div class="varietat"><?php the_field( 'varietat' ); ?></div>
                        <div class="sol"><?php the_field( 'tipus_de_sol' ); ?></div>
                    </div>
                </a>
                <?php endwhile; ?>
                <?php endif; wp_reset_query(); ?>
            </div>
            
        </div>
        
        
        <div class="row text-center">
            <div class="column column-20"></div>
            <div class="column">
                <a class="button" href="<?php echo esc_url( home_url() ); ?>">Pàgina d'inici</a>
            </div>
            <div class="column">
                <a class="button" href="<?php echo esc_url( home_url() ); ?>/reconeixements">Reconeixements</a>
            </div>
            <div class="column">
                <a class="button" href="<?php echo esc_url( home_url() ); ?>/contacte">Contacta'ns</a>
            </div>
            <div class="column column-20"></div>
        </div>
        
        
    </section>
    
</main>


<?php get_footer(); ?>
