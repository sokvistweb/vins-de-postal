<?php /* Template Name: Page Reconeixements */ get_header(); ?>


<main role="main" id="maincontent" class="wrapper">
    
    <section class="container container-x-narrow padding-x-short">
        <!-- This is the page title (any page using Default templagte, as this is page.php template) -->
        <h2 class="title text-center"><?php the_title(); ?></h2>
        
        <div class="row">
            <div class="column jenna-sue text-center">
                <!-- The Loop - Check if there are posts -->
                <?php if ( have_posts()) : while ( have_posts() ) : the_post(); ?>
                <!-- Dispaly the content of the current post -->
                <?php the_content(); ?>
                
                <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>
        
        
        <div class="row">
            <div class="column">
                
                <div class="">
                    <?php if (have_posts()) : ?>
                    <?php query_posts(array(
                        'post_type' => 'reconeixements',
                        'posts_per_page' => -1
                    )); ?>
                    <ul class="recon-list">
                        <?php while (have_posts()) : the_post(); ?>
                        <li class="card-member">
                            <div class="card-image">
                                <?php if ( has_post_thumbnail() ) : // Check if Thumbnail exists. ?>
                                <?php the_post_thumbnail('award', array( 'class' => 'img-responsive' )); // Fullsize image for the single post. ?>
                                <?php else : // Placeholder image if there is no Thumbnail ?>
                                <img class="img-responsive" src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/avatar-200.jpg" alt="" width="200" height="200">
                                <?php endif; ?>
                            </div>
                            <div class="card-content">
                                <div class="card-header">
                                    <h3><?php the_title(); ?></h3>

                                </div>
                                <div class="card-body">
                                    <?php the_content(); ?>
                                    
                                    <?php if ( have_rows( 'link_reconeixement2' ) ) : ?>
	                                <?php while ( have_rows( 'link_reconeixement2' ) ) : the_row(); ?>
                                        <?php if( get_sub_field('link_url') ): ?>
                                        <a class="recon-link" href="<?php the_sub_field( 'link_url' ); ?>" target="_blank"><?php the_sub_field( 'link_contingut' ); ?></a>
                                        <?php endif; ?>
                                    <?php endwhile; ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </li>
                        <?php endwhile; ?>
                    </ul>
                    <?php endif; wp_reset_query(); ?>
                </div>
                
            </div>
        </div>
        
        
        <div class="row text-center">
            <div class="column column-7"></div>
            <div class="column">
                <a class="button" href="<?php echo esc_url( home_url() ); ?>">Pàgina d'inici</a>
            </div>
            <div class="column">
                <a class="button" href="<?php echo esc_url( home_url() ); ?>/cementiri-de-postals">Cementiri de Postals</a>
            </div>
            <div class="column">
                <a class="button" href="<?php echo esc_url( home_url() ); ?>/contacte" title="On podeu trobar els vins de postal">Contacta'ns</a>
            </div>
            <div class="column column-7"></div>
        </div>
        
        
    </section>
    
</main>


<?php get_footer(); ?>
