<?php get_header(); ?>


<main role="main" id="maincontent" class="wrapper">
    
    <section class="container container-x-narrow padding-short">
        
        <h2 class="title text-center"><?php the_title(); ?></h2>
        
        <?php if ( have_posts() ) : while (have_posts() ) : the_post(); ?>
        <div class="row">
            <div class="column column-40 text-center">
                
                <div class="postal-content-left">
                    
                    <?php
                    $foto_ampolla = get_field( 'foto_ampolla' );
                    if( $foto_ampolla ): 
                        // Image variables.
                        $title = $foto_ampolla['title'];
                        $alt = $foto_ampolla['alt'];
                        // Thumbnail size attributes.
                        $size = 'medium';
                        $thumb = $foto_ampolla['sizes'][ $size ];
                        $width = $foto_ampolla['sizes'][ $size . '-width' ];
                        $height = $foto_ampolla['sizes'][ $size . '-height' ];
                    ?>
                    <img class="bottle" loading="lazy" src="<?php echo esc_url($thumb); ?>" alt="<?php echo esc_attr($alt); ?>" width="<?php echo esc_attr($width); ?>" height="<?php echo esc_attr($height); ?>" />
                    <?php endif; ?>

                </div>
                
            </div>
            
            
            <div class="column column-offset-2">
                
                <div class="postal-content-right">
                    
                    

                    <?php if ( has_post_thumbnail() ) : // Check if Thumbnail exists. ?>
                    <?php the_post_thumbnail('postal_featured'); ?>
                    <?php endif; ?>
                    
                    
                    <?php
                    $foto_postal = get_field( 'foto_postal' );
                    if( $foto_postal ): 
                        // Image variables.
                        $title = $foto_postal['title'];
                        $alt = $foto_postal['alt'];
                        // Thumbnail size attributes.
                        $size = 'medium_large';
                        $thumb = $foto_postal['sizes'][ $size ];
                        $width = $foto_postal['sizes'][ $size . '-width' ];
                        $height = $foto_postal['sizes'][ $size . '-height' ];
                    ?>
                    <img class="bg-card" loading="lazy" src="<?php echo esc_url($thumb); ?>" alt="<?php echo esc_attr($alt); ?>" width="<?php echo esc_attr($width); ?>" height="<?php echo esc_attr($height); ?>" />
                    <?php endif; ?>
                    
                    
                    
                    <h3>Fitxa tècnica</h3>

                    <dl>
                        <dt>Nom</dt>
                        <dd>Vins de Postal - <?php the_title(); ?></dd>
                        
                        <?php if (get_field('anyada')): ?>
                        <dt>Anyada</dt>
                        <dd><?php the_field( 'anyada' ); ?></dd>
                        <?php endif; ?>
                        
                        <?php if (get_field('finca')): ?>
                        <dt>Finca</dt>
                        <dd><?php the_field( 'finca' ); ?></dd>
                        <?php endif; ?>
                        
                        <?php if (get_field('municipi')): ?>
                        <dt>Municipi</dt>
                        <dd><?php the_field( 'municipi' ); ?></dd>
                        <?php endif; ?>
                        
                        <?php if (get_field('superficie')): ?>
                        <dt>Superfície</dt>
                        <dd><?php the_field( 'superficie' ); ?></dd>
                        <?php endif; ?>
                        
                        <?php if (get_field('altitud')): ?>
                        <dt>Altitud</dt>
                        <dd><?php the_field( 'altitud' ); ?></dd>
                        <?php endif; ?>
                        
                        <?php if (get_field('orientacioa_parcela')): ?>
                        <dt>Orientació de la parcel·la</dt>
                        <dd><?php the_field( 'orientacioa_parcela' ); ?></dd>
                        <?php endif; ?>
                        
                        <?php if (get_field('tipus_de_sol')): ?>
                        <dt>Tipus de sòl</dt>
                        <dd><?php the_field( 'tipus_de_sol' ); ?></dd>
                        <?php endif; ?>
                        
                        <?php if (get_field('varietat')): ?>
                        <dt>Varietat</dt>
                        <dd><?php the_field( 'varietat' ); ?></dd>
                        <?php endif; ?>
                        
                        <?php if (get_field('any_de_plantacio')): ?>
                        <dt>Any de plantació</dt>
                        <dd><?php the_field( 'any_de_plantacio' ); ?></dd>
                        <?php endif; ?>
                        
                        <?php if (get_field('conduccio')): ?>
                        <dt>Conducció</dt>
                        <dd><?php the_field( 'conduccio' ); ?></dd>
                        <?php endif; ?>
                        
                        <?php if (get_field('viticultor')): ?>
                        <dt>Viticultor</dt>
                        <dd><?php the_field( 'viticultor' ); ?></dd>
                        <?php endif; ?>
                        
                        <?php if (get_field('meteorologia')): ?>
                        <dt>Meteorologia</dt>
                        <dd><?php the_field( 'meteorologia' ); ?></dd>
                        <?php endif; ?>
                        
                        <?php if (get_field('verema')): ?>
                        <dt>Verema</dt> 
                        <dd><?php the_field( 'verema' ); ?></dd>
                        <?php endif; ?>
                        
                        <?php if (get_field('produccio')): ?>
                        <dt>Producció (rendiment)</dt>
                        <dd><?php the_field( 'produccio' ); ?></dd>
                        <?php endif; ?>
                        
                        <?php if (get_field('elaboracio')): ?>
                        <dt>Elaboració</dt>
                        <dd><?php the_field( 'elaboracio' ); ?></dd>
                        <?php endif; ?>
                        
                        <?php if (get_field('data_dembotellat')): ?>
                        <dt>Data d'embotellat</dt>
                        <dd><?php the_field( 'data_dembotellat' ); ?></dd>
                        <?php endif; ?>
                        
                        <?php if (get_field('ampolles')): ?>
                        <dt>Nº ampolles</dt>
                        <dd><?php the_field( 'ampolles' ); ?></dd>
                        <?php endif; ?>
                        
                        <?php if ( have_rows( 'analisi_fisico-quimics' ) ) : ?>
	                    <?php while ( have_rows( 'analisi_fisico-quimics' ) ) : the_row(); ?>
                        <?php if ( get_sub_field('ph') || get_sub_field('acidesa_total') || get_sub_field('grau_alcoholic') || get_sub_field('sucres_reductors') || get_sub_field('acidesa_volatil') ): ?>
                        <dt>Anàlisi físico-químics</dt>
                        <dd>
                            <dl>
                                <?php if (get_sub_field('ph')): ?>
                                <dt>PH</dt>
                                <dd><?php the_sub_field( 'ph' ); ?></dd>
                                <?php endif; ?>
                                
                                <?php if (get_sub_field('acidesa_total')): ?>
                                <dt>Acidesa total</dt>
                                <dd><?php the_sub_field( 'acidesa_total' ); ?></dd>
                                <?php endif; ?>
                                
                                <?php if (get_sub_field('grau_alcoholic')): ?>
                                <dt>Grau alcohólic</dt>
                                <dd><?php the_sub_field( 'grau_alcoholic' ); ?></dd>
                                <?php endif; ?>
                                
                                <?php if (get_sub_field('sucres_reductors')): ?>
                                <dt>Sucres reductors</dt>
                                <dd><?php the_sub_field( 'sucres_reductors' ); ?></dd>
                                <?php endif; ?>
                                
                                <?php if (get_sub_field('acidesa_volatil')): ?>
                                <dt>Acidesa volàtil</dt>
                                <dd><?php the_sub_field( 'acidesa_volatil' ); ?></dd>
                                <?php endif; ?>
                            </dl>
                        </dd>
                        <?php endif; ?>
                        <?php endwhile; ?>
                        <?php endif; ?>
                        
                        <?php if (get_field('temperatura_de_consum')): ?>
                        <dt>Temperatura de consum</dt>
                        <dd><?php the_field( 'temperatura_de_consum' ); ?></dd>
                        <?php endif; ?>
                        
                        <?php if ( have_rows( 'altres_dades_dinteres' ) ) : ?>
	                    <?php while ( have_rows( 'altres_dades_dinteres' ) ) : the_row(); ?>
                        <?php if ( get_sub_field('ampolla') || get_sub_field('tap') || get_sub_field('etiqueta') ): ?>
                        <dt>Altres dades d'interès</dt>
                        <dd>
                            <dl>
                                <?php if (get_sub_field('ampolla')): ?>
                                <dt>Ampolla</dt>
                                <dd><?php the_sub_field( 'ampolla' ); ?></dd>
                                <?php endif; ?>
                                
                                <?php if (get_sub_field('tap')): ?>
                                <dt>Tap</dt>
                                <dd><?php the_sub_field( 'tap' ); ?></dd>
                                <?php endif; ?>
                                
                                <?php if (get_sub_field('etiqueta')): ?>
                                <dt>Etiqueta</dt>
                                <dd><?php the_sub_field( 'etiqueta' ); ?></dd>
                                <?php endif; ?>
                            </dl>
                        </dd>
                        <?php endif; ?>
                        <?php endwhile; ?>
                        <?php endif; ?>
                    </dl>
                    
                    
                    <?php the_content(); // Content editor ?>
                    
                    
                </div>
                
            </div>
        </div>
        <?php endwhile; ?>
        <?php endif; ?>
        
                
    </section>
    
    
    <section class="container container-x-narrow padding-short">
        <div class="row text-center">
            <div class="column column-7"></div>
            <div class="column">
                <a class="button" href="<?php echo esc_url( home_url() ); ?>">Pàgina d'inici</a>
            </div>
            <div class="column">
                <a class="button" href="<?php echo esc_url( home_url() ); ?>/cementiri-de-postals">Cementiri de Postals</a>
            </div>
            <div class="column">
                <a class="button" href="<?php echo esc_url( home_url() ); ?>/contacte" title="On podeu trobar els vins de postal">Contacta'ns</a>
            </div>
            <div class="column column-7"></div>
        </div>
    </section>
    
    
</main>


<?php get_footer(); ?>
