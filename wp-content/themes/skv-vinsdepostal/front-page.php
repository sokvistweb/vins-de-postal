<?php get_header(); ?>


<!-- main content -->
<main class="sk-main">
    
    <div class="postcards clearfix">
        <div class="grid">
            <?php if (have_posts()) : ?>
            <?php query_posts(array(
                'post_type' => 'postals',
                'posts_per_page' => -1,
                'tax_query' => array(
                    array (
                        'taxonomy' => 'postals_cats',
                        'field' => 'slug',
                        'terms' => 'cementiri',
                        'operator' => 'NOT IN'
                    )
                )
            )); ?>
            <?php while (have_posts()) : the_post(); ?>
            <a href="<?php the_permalink(); ?>">
                <figure class="two-cols">
                <?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
                    <?php the_post_thumbnail('postal_featured', array( 
                    'loading' => 'eager'
                    )); ?>
                <?php endif; ?>
                    <div class="img-overlay"><span>+</span></div>
                </figure>
            </a>
            <?php endwhile; ?>
            <?php endif; wp_reset_query(); ?>
        </div>
    </div>
    
    
    <div class="text-center">
        <a class="button" href="<?php echo esc_url( home_url() ); ?>/cementiri-de-postals">Cementiri de Postals</a>
    </div>
    
    <div class="text-center">
        <a class="button button-outline" href="<?php echo esc_url( home_url() ); ?>/reconeixements">Reconeixements</a>
    </div>
    
    
    <?php if ( have_posts()) : while ( have_posts() ) : the_post(); ?>
    <div class="concept">
        <?php the_content(); ?>
    </div>
    <?php endwhile; ?>
    <?php endif; wp_reset_query(); ?>
    

    <div class="text-center">
        <a class="button" href="<?php echo esc_url( home_url() ); ?>/contacte" title="On podeu trobar els vins de postal">Contacta'ns</a>
    </div>
    
    
</main>


<?php get_footer('home'); ?>
