<?php get_header(); ?>


<main role="main" id="maincontent" class="wrapper notfound">
    
    <section class="container container-x-narrow padding-short">
        
        <h1 class="text-center">Pàgina no trobada</h1>
        
        <div class="row">
            <div class="column">
                <p>Ho sentim, però la pàgina que estàveu intentant veure no existeix.</p>
                
                <a class="button button-outline" href="<?php echo esc_url( home_url() ); ?>">Tornar a l'inici?</a>
            </div>
        </div>  
        
    </section>
    
</main>


<?php get_footer(); ?>
