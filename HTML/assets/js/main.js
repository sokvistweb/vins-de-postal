/* =============================================================================
   jQuery: all the custom stuff!
   ========================================================================== */

jQuery(document).ready(function($){

	//// Show/hide footer 
	$(window).scroll(function(){
    	if ($(window).scrollTop() + $(window).height() >= $(document).height() - 120) {
    		$('.sk-footer').addClass('footer-up');
    	} else {
     		$('.sk-footer').removeClass('footer-up');
    	}
	});
    
    
    
    
    // Venebox
    $('.venobox').venobox({
        framewidth: '700px',
        frameheight: '100%',
        overlayColor: '#0b1e22'
    });
    /*var venoOptions = {

        // is called after plugin initialization.
        cb_init: function(plugin){
            console.log('INIT');
            console.log(plugin);
        },

        // is called before the venobox pops up, return false to prevent opening;
        cb_pre_open : function(obj){
           console.log('PRE OPEN');
           console.log(obj);
        },

        // is called when opening is finished
        cb_post_open  : function(obj, gallIndex, thenext, theprev){
            console.log('POST OPEN');
            console.log('current gallery index: ' + gallIndex);
            console.log(thenext);
            console.log(theprev);
        }
    }
    $('.venobox').venobox(venoOptions);*/
    
    
	
	// Lazy load
    /*$('.lazy').Lazy({
        scrollDirection: 'vertical',
        //effect: 'fadeIn',
        //effectTime: 900
        delay: 5000,
        // called after an element was successfully handled
        afterLoad: function(element) {
            var imageSrc = element.data('src');
            console.log('image "' + imageSrc + '" was loaded successfully');
        },

        // called whenever an element could not be handled
        onError: function(element) {
            var imageSrc = element.data('src');
            console.log('image "' + imageSrc + '" could not be loaded');
        },

        // called once all elements was handled
        onFinishedAll: function() {
            console.log('finished loading all images');
        }
    });*/
    
});
