// Avoid `console` errors in browsers that lack a console.
(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());

// Place any jQuery/helper plugins in here.
///////////////////////////////////////////

/*
 * Lazy Line Painter
 * SVG Stroke animation.
 *
 * https://github.com/camoconnell/lazy-line-painter
 * http://www.camoconnell.com
 *
 * Licensed under the MIT license.
 *
 */
(function(e){var g={init:function(a){return this.each(function(){var b=e(this),c=b.data("lazyLinePainter");b.addClass("lazy-line");if(!c){var c=e.extend({width:null,height:null,strokeWidth:2,strokeColor:"#000",strokeOverColor:null,strokeCap:"round",strokeJoin:"round",strokeOpacity:1,arrowEnd:"none",onComplete:null,onStart:null,delay:null,overrideKey:null,drawSequential:!0,speedMultiplier:1,reverse:!1,responsive:!1},a),d=c.overrideKey?c.overrideKey:b.attr("id").replace("#",""),f=c.svgData[d].dimensions.width,
l=c.svgData[d].dimensions.height;c.svgData=c.svgData[d].strokepath;null===c.width&&(c.width=f);null===c.height&&(c.height=l);c.responsive||b.css({width:c.width,height:c.height});d="0 0 "+f+" "+l;f=document.createElementNS("http://www.w3.org/2000/svg","svg");f.setAttributeNS(null,"viewBox",d);f.setAttribute("xmlns","http://www.w3.org/2000/svg");c.svg=e(f);b.append(c.svg);b.data("lazyLinePainter",c)}})},paint:function(){return this.each(function(){var a=e(this).data("lazyLinePainter"),b=function(){a.paths=
[];a.longestDuration=0;for(var b=a.playhead=0,d=0,f=0,d=0;d<a.svgData.length;d++)b=a.svgData[d].duration*a.speedMultiplier,f+=b;for(d=0;d<a.svgData.length;d++){var e=m(a,d),h=e.getTotalLength();e.style.strokeDasharray=h+" "+h;e.style.strokeDashoffset=h;e.style.display="block";e.getBoundingClientRect();b=a.svgData[d].duration*a.speedMultiplier;b>a.longestDuration&&(a.longestDuration=b);var g;g=a.reverse?f-=b:a.playhead;a.paths.push({duration:b,drawStartTime:g,path:e,length:h});a.playhead+=b}a.totalDuration=
a.drawSequential?a.playhead:a.longestDuration;a.rAF=requestAnimationFrame(function(b){k(b,a)});if(null!==a.onStart)a.onStart()};null===a.delay?b():setTimeout(b,a.delay)})},pauseResume:function(){return this.each(function(){var a=e(this).data("lazyLinePainter");a.paused?(a.paused=!1,requestAnimationFrame(function(b){n(b,a)})):(a.paused=!0,cancelAnimationFrame(a.rAF))})},erase:function(){return this.each(function(){var a=e(this).data("lazyLinePainter");a.startTime=null;a.elapsedTime=null;cancelAnimationFrame(a.rAF);
a.svg.empty()})},destroy:function(){return this.each(function(){var a=e(this);a.removeData("lazyLinePainter");a.remove()})}},n=function(a,b){b.startTime=a-b.elapsedTime;requestAnimationFrame(function(a){k(a,b)})},k=function(a,b){b.startTime||(b.startTime=a);b.elapsedTime=a-b.startTime;for(var c=0;c<b.paths.length;c++){var d;b.drawSequential?(d=b.elapsedTime-b.paths[c].drawStartTime,0>d&&(d=0)):d=b.elapsedTime;d<b.paths[c].duration&&0<d?(d=d/b.paths[c].duration*b.paths[c].length,b.paths[c].path.style.strokeDashoffset=
b.reverse||b.svgData[c].reverse?-b.paths[c].length+d:b.paths[c].length-d):d>b.paths[c].duration&&(b.paths[c].path.style.strokeDashoffset=0)}if(b.elapsedTime<b.totalDuration)b.rAF=requestAnimationFrame(function(a){k(a,b)});else if(null!==b.onComplete)b.onComplete()},m=function(a,b){var c=document.createElementNS("http://www.w3.org/2000/svg","path"),d=e(c);a.svg.append(d);d.attr(p(a,a.svgData[b]));return c},p=function(a,b){return{d:b.path,stroke:b.strokeColor?b.strokeColor:a.strokeColor,"fill-opacity":0,
"stroke-opacity":b.strokeOpacity?b.strokeOpacity:a.strokeOpacity,"stroke-width":b.strokeWidth?b.strokeWidth:a.strokeWidth,"stroke-linecap":b.strokeCap?b.strokeCap:a.strokeCap,"stroke-linejoin":b.strokeJoin?b.strokeJoin:a.strokeJoin}};e.fn.lazylinepainter=function(a){if(g[a])return g[a].apply(this,Array.prototype.slice.call(arguments,1));if("object"!==typeof a&&a)console.log("opps - issue finding method");else return g.init.apply(this,arguments)}})(jQuery);


/* 
 * VenoBox - jQuery Plugin
 * version: 1.5.2
 * @requires jQuery
 *
 * Examples at http://lab.veno.it/venobox/
 * License: Creative Commons Attribution 3.0 License
 * License URI: http://creativecommons.org/licenses/by/3.0/
 * Copyright 2013-2014 Nicola Franchini - @nicolafranchini
 *
 */
(function(e){function j(){e.ajax({url:y,cache:false}).done(function(e){m.html('<div class="vbox-inline">'+e+"</div>");z(true)}).fail(function(){m.html('<div class="vbox-inline"><p>Error retrieving contents, please retry</div>');z(true)})}function F(){m.html('<iframe class="venoframe" src="'+y+'"></iframe>');z()}function I(){var e=y.split("/");var t=e[e.length-1];m.html('<iframe class="venoframe" src="//player.vimeo.com/video/'+t+'"></iframe>');z()}function q(){var e=y.split("/");var t=e[e.length-1];m.html('<iframe class="venoframe" allowfullscreen src="//www.youtube.com/embed/'+t+'"></iframe>');z()}function R(){m.html('<div class="vbox-inline">'+e(y).html()+"</div>");z()}function U(){w=e(".vbox-content").find("img");w.one("load",function(){z()}).each(function(){if(this.complete)e(this).load()})}function z(t){t=t||false;if(t!=true){e(window).scrollTop(0)}O.html(C);m.find(">:first-child").addClass("figlio");e(".figlio").css("width",s).css("height",a).css("padding",o).css("background",u);l=m.outerHeight();c=e(window).height();if(l+80<c){f=(c-l)/2;m.css("margin-top",f);m.css("margin-bottom",f)}else{m.css("margin-top","40px");m.css("margin-bottom","40px")}m.animate({opacity:"1"},"slow")}function W(){if(e(".vbox-content").length){l=m.height();c=e(window).height();if(l+80<c){f=(c-l)/2;m.css("margin-top",f);m.css("margin-bottom",f)}else{m.css("margin-top","40px");m.css("margin-bottom","40px")}}}var t,n,r,i,s,o,u,a,f,l,c,h,p,d,v,m,g,y,b,w,E,S,x,T,N,C,k,L,A,O,M,_,D,P,H,B;e.fn.extend({venobox:function(f){var l={framewidth:"",frameheight:"",border:"0",bgcolor:"#fff",titleattr:"title",numeratio:false,infinigall:false,overlayclose:true};var c=e.extend(l,f);return this.each(function(){var f=e(this);if(f.data("venobox")){return true}f.addClass("vbox-item");f.data("framewidth",c.framewidth);f.data("frameheight",c.frameheight);f.data("border",c.border);f.data("bgcolor",c.bgcolor);f.data("numeratio",c.numeratio);f.data("infinigall",c.infinigall);f.data("overlayclose",c.overlayclose);f.data("venobox",true);t=navigator.userAgent.match(/(iPad|iPhone|iPod)/g)?true:false;n=document.all&&!window.atob?true:false;f.click(function(l){function w(){S=f.data("gall");h=f.data("numeratio");v=f.data("infinigall");x=e('.vbox-item[data-gall="'+S+'"]');if(x.length>0&&h===true){M.html(x.index(f)+1+" / "+x.length);M.fadeIn()}else{M.fadeOut()}T=x.eq(x.index(f)+1);N=x.eq(x.index(f)-1);if(f.attr(c.titleattr)){C=f.attr(c.titleattr);O.fadeIn()}else{C="";O.fadeOut()}if(x.length>0&&v===true){k=true;L=true;if(T.length<1){T=x.eq(0)}if(x.index(f)<1){N=x.eq(x.index(x.length))}}else{if(T.length>0){e(".vbox-next").css("display","block");k=true}else{e(".vbox-next").css("display","none");k=false}if(x.index(f)>0){e(".vbox-prev").css("display","block");L=true}else{e(".vbox-prev").css("display","none");L=false}}}function z(e){if(e.keyCode===27){W()}}function W(){e("body").unbind("keydown",z);if(n){i.animate({opacity:0},500,function(){i.remove();e(".vwrap").children().unwrap();e(window).scrollTop(-b);A=false;f.focus()})}else{i.unbind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd");i.bind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd",function(n){if(n.target!=n.currentTarget){return}i.remove();if(t){e(".vwrap").bind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd",function(){e(".vwrap").children().unwrap();e(window).scrollTop(-b)});e(".vwrap").css("opacity","1")}else{e(".vwrap").children().unwrap();e(window).scrollTop(-b)}A=false;f.focus()});i.css("opacity","0")}}l.stopPropagation();l.preventDefault();f=e(this);r=f.data("overlay");s=f.data("framewidth");a=f.data("frameheight");o=f.data("border");u=f.data("bgcolor");k=false;L=false;A=false;y=f.attr("href");b=e(window).scrollTop();b=-b;B=f.data("css")||"";e("body").wrapInner('<div class="vwrap"></div>');p=e(".vwrap");g='<div class="vbox-overlay '+B+'" style="background:'+r+'"><div class="vbox-preloader">Loading...</div><div class="vbox-container"><div class="vbox-content"></div></div><div class="vbox-title"></div><div class="vbox-num">0/0</div><div class="vbox-close">X</div><div class="vbox-next">next</div><div class="vbox-prev">prev</div></div>';e("body").append(g);i=e(".vbox-overlay");d=e(".vbox-container");m=e(".vbox-content");M=e(".vbox-num");O=e(".vbox-title");m.html("");m.css("opacity","0");w();i.css("min-height",e(window).outerHeight());if(n){i.animate({opacity:1},250,function(){i.css({"min-height":e(window).outerHeight(),height:"auto"});if(f.data("type")=="iframe"){F()}else if(f.data("type")=="inline"){R()}else if(f.data("type")=="ajax"){j()}else if(f.data("type")=="vimeo"){I()}else if(f.data("type")=="youtube"){q()}else{m.html('<img src="'+y+'">');U()}})}else{i.bind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd",function(t){if(t.target!=t.currentTarget){return}i.css({"min-height":e(window).outerHeight(),height:"auto"});if(f.data("type")=="iframe"){F()}else if(f.data("type")=="inline"){R()}else if(f.data("type")=="ajax"){j()}else if(f.data("type")=="vimeo"){I()}else if(f.data("type")=="youtube"){q()}else{m.html('<img src="'+y+'">');U()}});i.css("opacity","1")}if(t){p.css({position:"fixed",top:b,opacity:"0"}).data("top",b)}else{p.css({position:"fixed",top:b}).data("top",b);e(window).scrollTop(0)}var E={prev:function(){if(A)return;else A=true;r=N.data("overlay");s=N.data("framewidth");a=N.data("frameheight");o=N.data("border");u=N.data("bgcolor");y=N.attr("href");if(N.attr(c.titleattr)){C=N.attr(c.titleattr)}else{C=""}if(r===undefined){r=""}i.css("min-height",e(window).outerHeight());m.animate({opacity:0},500,function(){i.css("min-height",e(window).outerHeight()).css("background",r);if(N.data("type")=="iframe"){F()}else if(N.data("type")=="inline"){R()}else if(N.data("type")=="ajax"){j()}else if(N.data("type")=="youtube"){q()}else if(N.data("type")=="vimeo"){I()}else{m.html('<img src="'+y+'">');U()}f=N;w();A=false})},next:function(){if(A)return;else A=true;r=T.data("overlay");s=T.data("framewidth");a=T.data("frameheight");o=T.data("border");u=T.data("bgcolor");y=T.attr("href");if(T.attr(c.titleattr)){C=T.attr(c.titleattr)}else{C=""}if(r===undefined){r=""}i.css("min-height",e(window).outerHeight());m.animate({opacity:0},500,function(){i.css("min-height",e(window).outerHeight()).css("background",r);if(T.data("type")=="iframe"){F()}else if(T.data("type")=="inline"){R()}else if(T.data("type")=="ajax"){j()}else if(T.data("type")=="youtube"){q()}else if(T.data("type")=="vimeo"){I()}else{m.html('<img src="'+y+'">');U()}f=T;w();A=false})}};e("body").keydown(function(e){if(e.keyCode==37&&L==true){E.prev()}if(e.keyCode==39&&k==true){E.next()}});e(".vbox-prev").click(function(){E.prev()});e(".vbox-next").click(function(){E.next()});var X=".vbox-close, .vbox-overlay";if(!f.data("overlayclose")){X=".vbox-close"}e(X).click(function(t){P=".figlio";D=".vbox-prev";_=".vbox-next";H=".figlio *";if(!e(t.target).is(P)&&!e(t.target).is(_)&&!e(t.target).is(D)&&!e(t.target).is(H)){W()}});e("body").keydown(z);return false})})}});e(window).resize(function(){W()})})(jQuery);


/**
 *
 * JQUERY EU COOKIE LAW POPUPS
 * version 1.1.1
 *
 * Code on Github:
 * https://github.com/wimagguc/jquery-eu-cookie-law-popup
 *
 * To see a live demo, go to:
 * http://www.wimagguc.com/2018/05/gdpr-compliance-with-the-jquery-eu-cookie-law-plugin/
 *
 * by Richard Dancsi
 * http://www.wimagguc.com/
 *
 */

(function($) {

// for ie9 doesn't support debug console >>>
if (!window.console) window.console = {};
if (!window.console.log) window.console.log = function () { };
// ^^^

$.fn.euCookieLawPopup = (function() {

	var _self = this;

	///////////////////////////////////////////////////////////////////////////////////////////////
	// PARAMETERS (MODIFY THIS PART) //////////////////////////////////////////////////////////////
	_self.params = {
		cookiePolicyUrl : 'https://www.cellerespolla.com/politica-de-cookies/',
		popupPosition : 'top',
		colorStyle : 'default',
		compactStyle : false,
		popupTitle : 'This website is using cookies',
		popupText : 'We use cookies to ensure that we give you the best experience on our website. If you continue without changing your settings, we\'ll assume that you are happy to receive all cookies on this website.',
		buttonContinueTitle : 'Continue',
		buttonLearnmoreTitle : 'Learn&nbsp;more',
		buttonLearnmoreOpenInNewWindow : true,
		agreementExpiresInDays : 30,
		autoAcceptCookiePolicy : false,
		htmlMarkup : 'null'
	};

	///////////////////////////////////////////////////////////////////////////////////////////////
	// VARIABLES USED BY THE FUNCTION (DON'T MODIFY THIS PART) ////////////////////////////////////
	_self.vars = {
		INITIALISED : false,
		HTML_MARKUP : null,
		COOKIE_NAME : 'EU_COOKIE_LAW_CONSENT'
	};

	///////////////////////////////////////////////////////////////////////////////////////////////
	// PRIVATE FUNCTIONS FOR MANIPULATING DATA ////////////////////////////////////////////////////

	// Overwrite default parameters if any of those is present
	var parseParameters = function(object, markup, settings) {

		if (object) {
			var className = $(object).attr('class') ? $(object).attr('class') : '';
			if (className.indexOf('eupopup-top') > -1) {
				_self.params.popupPosition = 'top';
			}
			else if (className.indexOf('eupopup-fixedtop') > -1) {
				_self.params.popupPosition = 'fixedtop';
			}
			else if (className.indexOf('eupopup-bottomright') > -1) {
				_self.params.popupPosition = 'bottomright';
			}
			else if (className.indexOf('eupopup-bottomleft') > -1) {
				_self.params.popupPosition = 'bottomleft';
			}
			else if (className.indexOf('eupopup-bottom') > -1) {
				_self.params.popupPosition = 'bottom';
			}
			else if (className.indexOf('eupopup-block') > -1) {
				_self.params.popupPosition = 'block';
			}
			if (className.indexOf('eupopup-color-default') > -1) {
				_self.params.colorStyle = 'default';
			}
			else if (className.indexOf('eupopup-color-inverse') > -1) {
				_self.params.colorStyle = 'inverse';
			}
			if (className.indexOf('eupopup-style-compact') > -1) {
				_self.params.compactStyle = true;
			}
		}

		if (markup) {
			_self.params.htmlMarkup = markup;
		}

		if (settings) {
			if (typeof settings.cookiePolicyUrl !== 'undefined') {
				_self.params.cookiePolicyUrl = settings.cookiePolicyUrl;
			}
			if (typeof settings.popupPosition !== 'undefined') {
				_self.params.popupPosition = settings.popupPosition;
			}
			if (typeof settings.colorStyle !== 'undefined') {
				_self.params.colorStyle = settings.colorStyle;
			}
			if (typeof settings.popupTitle !== 'undefined') {
				_self.params.popupTitle = settings.popupTitle;
			}
			if (typeof settings.popupText !== 'undefined') {
				_self.params.popupText = settings.popupText;
			}
			if (typeof settings.buttonContinueTitle !== 'undefined') {
				_self.params.buttonContinueTitle = settings.buttonContinueTitle;
			}
			if (typeof settings.buttonLearnmoreTitle !== 'undefined') {
				_self.params.buttonLearnmoreTitle = settings.buttonLearnmoreTitle;
			}
			if (typeof settings.buttonLearnmoreOpenInNewWindow !== 'undefined') {
				_self.params.buttonLearnmoreOpenInNewWindow = settings.buttonLearnmoreOpenInNewWindow;
			}
			if (typeof settings.agreementExpiresInDays !== 'undefined') {
				_self.params.agreementExpiresInDays = settings.agreementExpiresInDays;
			}
			if (typeof settings.autoAcceptCookiePolicy !== 'undefined') {
				_self.params.autoAcceptCookiePolicy = settings.autoAcceptCookiePolicy;
			}
			if (typeof settings.htmlMarkup !== 'undefined') {
				_self.params.htmlMarkup = settings.htmlMarkup;
			}
		}

	};

	var createHtmlMarkup = function() {

		if (_self.params.htmlMarkup) {
			return _self.params.htmlMarkup;
		}

		var html =
			'<div class="eupopup-container' +
			    ' eupopup-container-' + _self.params.popupPosition +
			    (_self.params.compactStyle ? ' eupopup-style-compact' : '') +
				' eupopup-color-' + _self.params.colorStyle + '">' +
				'<div class="eupopup-head">' + _self.params.popupTitle + '</div>' +
				'<div class="eupopup-body">' + _self.params.popupText + '</div>' +
				'<div class="eupopup-buttons">' +
				  '<a href="#" class="eupopup-button eupopup-button_1">' + _self.params.buttonContinueTitle + '</a>' +
				  '<a href="' + _self.params.cookiePolicyUrl + '"' +
				 	(_self.params.buttonLearnmoreOpenInNewWindow ? ' target=_blank ' : '') +
					' class="eupopup-button eupopup-button_2">' + _self.params.buttonLearnmoreTitle + '</a>' +
				  '<div class="clearfix"></div>' +
				'</div>' +
				'<a href="#" class="eupopup-closebutton">x</a>' +
			'</div>';

		return html;
	};

	// Storing the consent in a cookie
	var setUserAcceptsCookies = function(consent) {
		var d = new Date();
		var expiresInDays = _self.params.agreementExpiresInDays * 24 * 60 * 60 * 1000;
		d.setTime( d.getTime() + expiresInDays );
		var expires = "expires=" + d.toGMTString();
		document.cookie = _self.vars.COOKIE_NAME + '=' + consent + "; " + expires + ";path=/";

		$(document).trigger("user_cookie_consent_changed", {'consent' : consent});
	};

	// Let's see if we have a consent cookie already
	var userAlreadyAcceptedCookies = function() {
		var userAcceptedCookies = false;
		var cookies = document.cookie.split(";");
		for (var i = 0; i < cookies.length; i++) {
			var c = cookies[i].trim();
			if (c.indexOf(_self.vars.COOKIE_NAME) == 0) {
				userAcceptedCookies = c.substring(_self.vars.COOKIE_NAME.length + 1, c.length);
			}
		}

		return userAcceptedCookies;
	};

	var hideContainer = function() {
		// $('.eupopup-container').slideUp(200);
		$('.eupopup-container').animate({
			opacity: 0,
			height: 0
		}, 200, function() {
			$('.eupopup-container').hide(0);
		});
	};

	///////////////////////////////////////////////////////////////////////////////////////////////
	// PUBLIC FUNCTIONS  //////////////////////////////////////////////////////////////////////////
	var publicfunc = {

		// INITIALIZE EU COOKIE LAW POPUP /////////////////////////////////////////////////////////
		init : function(settings) {

			parseParameters(
				$(".eupopup").first(),
				$(".eupopup-markup").html(),
				settings);

			// No need to display this if user already accepted the policy
			if (userAlreadyAcceptedCookies()) {
        $(document).trigger("user_cookie_already_accepted", {'consent': true});
				return;
			}

			// We should initialise only once
			if (_self.vars.INITIALISED) {
				return;
			}
			_self.vars.INITIALISED = true;

			// Markup and event listeners >>>
			_self.vars.HTML_MARKUP = createHtmlMarkup();

			if ($('.eupopup-block').length > 0) {
				$('.eupopup-block').append(_self.vars.HTML_MARKUP);
			} else {
				$('BODY').append(_self.vars.HTML_MARKUP);
			}

			$('.eupopup-button_1').click(function() {
				setUserAcceptsCookies(true);
				hideContainer();
				return false;
			});
			$('.eupopup-closebutton').click(function() {
				setUserAcceptsCookies(true);
				hideContainer();
				return false;
			});
			// ^^^ Markup and event listeners

			// Ready to start!
			$('.eupopup-container').show();

			// In case it's alright to just display the message once
			if (_self.params.autoAcceptCookiePolicy) {
				setUserAcceptsCookies(true);
			}

		}

	};

	return publicfunc;
});

$(document).ready( function() {
	if ($(".eupopup").length > 0) {
		$(document).euCookieLawPopup().init({
			'info' : 'YOU_CAN_ADD_MORE_SETTINGS_HERE',
			'popupTitle' : '',
            'popupText' : '',
            'buttonContinueTitle' : '',
            'buttonLearnmoreTitle' : '',
            'popupPosition' : 'hide',
            'htmlMarkup' : ''
		});
	}
});

$(document).bind("user_cookie_consent_changed", function(event, object) {
	console.log("User cookie consent changed: " + $(object).attr('consent') );
});

}(jQuery));


